import pika
from decouple import config

from src.app.receiver import receiver


credentials = pika.PlainCredentials('learning', 'uZuBLIPlajH6gt3WV067')
parameters = pika.ConnectionParameters('localhost',
                                       5672,
                                       '/',
                                       credentials)
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='learning', exchange_type='topic', durable=True)

result = channel.queue_declare('', exclusive=False)
queue_name = result.method.queue

receiver(channel, queue_name)

try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()

connection.close()
