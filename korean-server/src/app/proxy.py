import yaml
import json
import re

import graphene

from sqlalchemy import create_engine
from sqlalchemy.orm import (scoped_session, sessionmaker)

from .queries.user_query import UserQuery, UserMutations
from .queries.token_query import TokenQuery, TokenMutations
from .queries.verify_email_query import VerifyEmailQuery
from .queries.reset_password_email_query import ResetPasswordEmailQuery

from .subscriber import subscriber


engine = create_engine('sqlite:///test.db')
session = scoped_session(sessionmaker(bind=engine))


def proxy(channel, method_frame, header_frame, body):
    token = header_frame.headers.get("access_token")
    if (token == None):
        token = ""
    
    try:
        if method_frame and method_frame.routing_key == 'in.login':
            schema = graphene.Schema(query=UserQuery)
            user = schema.execute(body, context_value={'session': session})
            yaml_user = yaml.load(str(user), Loader=yaml.SafeLoader)

            if yaml_user and yaml_user["data"]["userLogin"] != "None" and user:
                user_id = yaml_user["data"]["userLogin"]["id"]
                schema = graphene.Schema(query=TokenQuery, mutation=TokenMutations)

                mutation_create_query = \
    '''
    mutation {
        createTokens(userId: '''+user_id+''') {
            accessToken,
            refreshToken
        }
    }
    '''
                schema.execute(mutation_create_query,
                            context_value={'session': session})

                last_result_query = \
    '''
    query {
        token(userId: '''+user_id+''') {
            accessToken,
            refreshToken
        }
    }
    '''
                tokens = schema.execute(
                    last_result_query, context_value={'session': session})
                yaml_tokens = yaml.load(str(tokens), Loader=yaml.SafeLoader)

                yaml_tokens["data"].update({
                "userId": {
                    "id": user_id
                }
            })

                if yaml_tokens["data"]["token"] != "None":
                    subscriber(channel, 'out.login', str(yaml_tokens))
            else:
                subscriber(channel, 'out.login',
                        "{ \"data\": { \"errors\": { \"errorMessage\": \"Błędny login lub hasło\" ,\"errorCode\": 404 } } }")
        elif method_frame and method_frame.routing_key == 'in.register':
            schema = graphene.Schema(query=UserQuery, mutation=UserMutations)
            user = schema.execute(body, context_value={'session': session})
        elif method_frame and method_frame.routing_key == 'in.app.' + token + '.user':
            schema = graphene.Schema(query=UserQuery)
            result = schema.execute(body, context_value={'session': session})
            data = yaml.load(str(result), Loader=yaml.SafeLoader)

            data["data"].update({
                "validation": {
                    "token": token
                }
            })

            if data and data["data"]["user"] != "None" and result:
                subscriber(channel, 'out.app.' + token + '.user', str(data))
        elif method_frame and method_frame.routing_key == 'in.app.' + token + '.user.all':
            schema = graphene.Schema(query=UserQuery)
            result = schema.execute(body, context_value={'session': session})
            data = yaml.load(str(result), Loader=yaml.SafeLoader)

            data["data"].update({
                "validation": {
                    "token": token
                }
            })

            if data and data["data"]["users"] != "None" and result:
                subscriber(channel, 'out.app.' + token + '.user.all', str(data))
        elif method_frame and method_frame.routing_key == 'in.app.' + token + '.token.refresh':
            schema = graphene.Schema(query=TokenQuery, mutation=TokenMutations)
            result = schema.execute(body, context_value={'session': session})

            if result.data != None:
                yaml_result = yaml.load(json.dumps((result.data)), Loader=yaml.SafeLoader)

                yaml_tokens = {"data": yaml_result}

                yaml_tokens["data"].update({
                    "validation": {
                        "token": token
                    }
                })

                if yaml_tokens and yaml_tokens["data"]["refreshTokens"] != "None" and result:
                    subscriber(channel, 'out.app.' + token + '.token.refresh', str(yaml_tokens))
            else:
                subscriber(channel, 'out.app.errors', "{ \"data\": { \"errors\": { \"errorMessage\": \"Błąd tokena odświeżenia\" ,\"errorCode\": 401 } } }")
        elif method_frame and method_frame.routing_key == 'in.app.' + token + '.token.verify':
            schema = graphene.Schema(query=TokenQuery)
            result = schema.execute(body, context_value={'session': session}) 
            data = yaml.load(str(result), Loader=yaml.SafeLoader)
                    
            if data and data["data"]["verifyTokens"] != "None" and result:
                subscriber(channel, 'out.app.' + token + '.token.verify', str(data))
            else:
                subscriber(channel, 'out.app.errors', "{ \"data\": { \"errors\": { \"errorMessage\": \"Błąd tokena dostępu\" ,\"errorCode\": 401 } } }")
    except:
        pass
    return False
