import json

def callback(channel, method_frame, header_frame, body):
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

def subscriber(channel, key, data):
    print("OUT")
    print(key)
    print(data)
    channel.basic_publish(exchange='learning', routing_key=key, body=json.dumps(data))
