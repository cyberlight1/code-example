from datetime import datetime

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, DateTime, create_engine
from sqlalchemy.orm import relationship, backref

from sqlalchemy.ext.declarative import declarative_base

from .user import UserModel


Base = declarative_base()

class TokenModel(Base):
    __tablename__ = 'token'
    
    id = Column(Integer, primary_key=True)
    
    user = relationship(UserModel)
    user_id = Column(Integer, ForeignKey(UserModel.id))
    
    access_token = Column(String)
    refresh_token = Column(String)

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
