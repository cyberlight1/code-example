from datetime import datetime

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, DateTime, create_engine
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class UserModel(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)

    login = Column(String)
    email = Column(String)
    password = Column(String)
    avatar_link = Column(String)
    group = Column(String)
    team = Column(String)

    exp_number = Column(Integer)
    points_number = Column(Integer)
    premium_level = Column(Integer)

    is_verified_email = Column(Boolean)

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
