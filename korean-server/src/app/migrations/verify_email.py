from datetime import datetime

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, DateTime, create_engine
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from .user import UserModel


Base = declarative_base()

class VerifyEmailModel(Base):
    __tablename__ = 'verify_email'

    id = Column(Integer, primary_key=True)
    
    user = relationship(UserModel)
    user_id = Column('user_id', Integer, ForeignKey(UserModel.id))
    
    token = Column(String)

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
