import graphene
import hashlib
import uuid

from graphene_sqlalchemy import SQLAlchemyObjectType

from ..migrations.verify_email import VerifyEmailModel

class VerifyEmail(SQLAlchemyObjectType):
    class Meta:
        model = VerifyEmailModel

class VerifyEmailQuery(graphene.ObjectType):
    user_id = graphene.Field(VerifyEmail, user_id=graphene.String())

    def resolve_user_id(self, info, user_id):
        query = VerifyEmail.get_query(info)
        return query.filter(VerifyEmailModel.user_id == user_id).first()
