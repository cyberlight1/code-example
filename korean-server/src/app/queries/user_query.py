from decouple import config

import graphene
import hashlib


from graphene_sqlalchemy import SQLAlchemyObjectType

from ..migrations.user import UserModel
from ..migrations.token import TokenModel


ENV_SALT = config('ENV_SALT')


def hash_password(password):
    return hashlib.sha256(ENV_SALT.encode() + password.encode()).hexdigest() + ':' + ENV_SALT


class User(SQLAlchemyObjectType):
    class Meta:
        model = UserModel


class UserQuery(graphene.ObjectType):
    users = graphene.List(User)
    user = graphene.Field(User, id=graphene.Int(), login=graphene.String())

    user_login = graphene.Field(User, id=graphene.Int(
    ), login=graphene.String(), password=graphene.String())

    def resolve_users(self, info):
        query = User.get_query(info)

        return query.all()

    def resolve_user(self, info, login):
        query = User.get_query(info)
        result = query.filter(UserModel.login == login).first()
        return result

    def resolve_user_login(self, info, login, password):
        query = User.get_query(info)
        result = query.filter(
            UserModel.login == login, UserModel.password == hash_password(password)).first()
        return result


class CreateUser(graphene.Mutation):
    login = graphene.String()
    email = graphene.String()
    password = graphene.String()
    is_verified_email = graphene.Boolean()

    class Arguments:
        login = graphene.String(required=True)
        email = graphene.String(required=True)
        password = graphene.String(required=True)
        is_verified_email = graphene.Boolean()

    def mutate(self, info, **args):
        session = info.context.get('session')

        login = args.get('login')
        email = args.get('email')

        password = hash_password(args.get('password'))

        is_verified_email = False

        data = {"login": login, "email": email,
                "password": password, "is_verified_email": is_verified_email}

        session.add(UserModel(**data))
        session.commit()

        return CreateUser(**data)


class UserMutations(graphene.ObjectType):
    register_user = CreateUser.Field()
