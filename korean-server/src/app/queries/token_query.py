import graphene
import hashlib
import uuid
import json
import datetime

from decouple import config

from graphene_sqlalchemy import SQLAlchemyObjectType

from ..migrations.token import TokenModel

ENV_SALT = config('ENV_SALT')
ENV_ACCESS_TOKEN_TIME = json.loads(config('ENV_ACCESS_TOKEN_TIME'))
ENV_REFRESH_TOKEN_TIME = json.loads(config('ENV_REFRESH_TOKEN_TIME'))


def generate_token():
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode()).hexdigest()


class Token(SQLAlchemyObjectType):
    class Meta:
        model = TokenModel


class TokenQuery(graphene.ObjectType):
    token = graphene.Field(Token, user_id=graphene.Int(), access_token=graphene.String(), refresh_token=graphene.String())
    verify_tokens = graphene.Field(Token, access_token=graphene.String())

    id = graphene.Field(Token, id=graphene.String())
    user_id = graphene.Field(Token, user_id=graphene.String())

    def resolve_token(self, info, user_id):
        query = Token.get_query(info)

        now = datetime.datetime.utcnow()

        return query.filter(TokenModel.user_id == user_id, TokenModel.created_at + datetime.timedelta(**ENV_ACCESS_TOKEN_TIME) <= now).order_by(TokenModel.id.desc()).first()

    def resolve_verify_tokens(self, info, access_token):
        query = Token.get_query(info)

        now = datetime.datetime.utcnow()

        return query.filter(TokenModel.access_token == access_token, TokenModel.created_at + datetime.timedelta(**ENV_ACCESS_TOKEN_TIME) <= now).first()


class CreateTokens(graphene.Mutation):
    user_id = graphene.Int()
    access_token = graphene.String()
    refresh_token = graphene.String()

    class Arguments:
        user_id = graphene.Int(required=True)

    def mutate(self, info, **args):
        session = info.context.get('session')

        query = Token.get_query(info)
        
        user_id = args.get('user_id')

        record = query.filter(TokenModel.user_id == user_id).order_by(TokenModel.id.desc()).first()
        if record != None:
            query.filter(TokenModel.user_id == user_id, TokenModel.access_token == record.access_token).delete()

        access_token = generate_token()
        refresh_token = generate_token()

        data = {"user_id": user_id, "access_token": access_token,
                "refresh_token": refresh_token}

        session.add(TokenModel(**data))
        session.commit()

        return CreateTokens(**data)

class CleanTokens(graphene.Mutation):
    user_id = graphene.Int()

    class Arguments:
        user_id = graphene.Int(required=True)

    def mutate(self, info, **args):
        session = info.context.get('session')

        query = Token.get_query(info)

        user_id = args.get('user_id')

        query.filter(TokenModel.user_id == user_id).delete()
        session.commit()

        return CleanTokens(user_id=user_id)

class RefreshTokens(graphene.Mutation):
    user_id = graphene.Int()
    access_token = graphene.String()
    refresh_token = graphene.String()

    class Arguments:
        user_id = graphene.Int(required=True)
        access_token = graphene.String(required=True)
        refresh_token = graphene.String(required=True)

    def mutate(self, info, **args):
        session = info.context.get('session')

        query = Token.get_query(info)

        user_id = args.get('user_id')
        access_token = args.get('access_token')
        refresh_token = args.get('refresh_token')

        now = datetime.datetime.utcnow()
 
        if query.filter(TokenModel.refresh_token == refresh_token, TokenModel.created_at + datetime.timedelta(**ENV_REFRESH_TOKEN_TIME) <= now).first() != None:
            query.filter(TokenModel.refresh_token == refresh_token).delete()

            access_token = generate_token()
            refresh_token = generate_token()

            data = {"user_id": user_id, "access_token": access_token,
                    "refresh_token": refresh_token}

            session.add(TokenModel(**data))
            session.commit()

            return CreateTokens(**data)
        
        data = {"user_id": user_id, "access_token": access_token,
                "refresh_token": refresh_token}

        return CreateTokens(**data)

class TokenMutations(graphene.ObjectType):
    create_tokens = CreateTokens.Field()
    clean_tokens = CleanTokens.Field()
    refresh_tokens = RefreshTokens.Field()
