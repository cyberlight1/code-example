import graphene
import hashlib
import uuid

from graphene_sqlalchemy import SQLAlchemyObjectType

from ..migrations.reset_password_email import ResetPasswordEmailModel


class ResetPasswordEmail(SQLAlchemyObjectType):
    class Meta:
        model = ResetPasswordEmailModel

class ResetPasswordEmailQuery(graphene.ObjectType):
    user_id = graphene.Field(ResetPasswordEmail, email_user_id=graphene.String())

    def resolve_user_id(self, info, user_id):
        query = ResetPasswordEmail.get_query(info)
        return query.filter(ResetPasswordEmailModel.user_id == user_id).first()
