import time

from .proxy import proxy


def callback(channel, method_frame, header_frame, body):
    print("IN")
    print(method_frame.routing_key)
    print(body)
    body = body.decode('utf-8')
    proxy(channel, method_frame, header_frame, body)
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

def receiver(channel, queue_name):
    channel.queue_bind(exchange='learning', queue=queue_name, routing_key="in.#")

    channel.basic_consume(queue_name, callback)
