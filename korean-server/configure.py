import uuid
import json

salt = uuid.uuid4().hex

with open("./src/app/queries/.env", "w") as f:
    f.write("ENV_SALT=" + salt + "\n")
    f.write("ENV_ACCESS_TOKEN_TIME=" + json.dumps({"days":3}) + "\n")
    f.write("ENV_REFRESH_TOKEN_TIME=" + json.dumps({"days":14}) + "\n")
