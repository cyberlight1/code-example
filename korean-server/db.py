from sqlalchemy import create_engine

from src.app.migrations.user import UserModel
from src.app.migrations.token import TokenModel
# from src.login.migrations.verify_email import VerifyEmailModel
# from src.login.migrations.reset_password_email import ResetPasswordEmailModel


engine = create_engine('sqlite:///test.db')

UserModel.metadata.create_all(engine)
TokenModel.metadata.create_all(engine)
# VerifyEmailModel.metadata.create_all(engine)
# ResetPasswordEmailModel.metadata.create_all(engine)
