//RabbitMQ
const amqp = require('amqplib/callback_api');

const rabbitUrl = 'amqp://localhost';

const sendRabbitMQ = function sendRabbitMQ(key, data, headers) {
    amqp.connect(rabbitUrl, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }

            const exchange = "learning";

            channel.assertExchange(exchange, 'topic', {
                durable: true
            });

            channel.publish(exchange, key, Buffer.from(data), headers);
        });
        setTimeout(function () {
            connection.close();
        }, 500);
    });
}

module.exports = sendRabbitMQ;

