const io = require('./server');
const amqp = require('amqplib/callback_api');
const yaml = require('js-yaml');


amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        channel.assertQueue('', {
            exclusive: false
        }, function (error2, q) {
            if (error2) {
                throw error2;
            }
            const exchange = "learning";

            channel.bindQueue(q.queue, exchange, "out.login");
            channel.consume(q.queue, function (msg) {                
                const message = yaml.load(msg.content.toString('utf-8').slice(1,-1), 'utf8')

                if (message.data && message.data.errors && message.data.errors.errorMessage && message.data.errors.errorMessage.length > 0) {
                    io.socket.emit("app:errors", message);
                }

                if (message.data && message.data.token && msg.fields.routingKey == "out.login") {
                    if (message.data && !message.data.errors) {
                        channel.bindQueue(q.queue, exchange, "out.app." + message.data.token.accessToken + ".#");
                        io.socket.emit("app:tokens", message);
                    }
                }

                if (message.data && message.data.validation && msg.fields.routingKey === "out.app." + message.data.validation.token + ".user") {
                    io.socket.emit("app:users", message);
                }

                if (message.data && message.data.validation && msg.fields.routingKey === "out.app." + message.data.validation.token + ".user.all") {
                    io.socket.emit("app:users:all", message.data.users);
                }

                if (message.data && message.data.verifyTokens && message.data.verifyTokens.accessToken && msg.fields.routingKey === "out.app." + message.data.verifyTokens.accessToken + ".token.verify") {
                    io.socket.emit("app:token:access", message);
                }

                if (message.data && message.data.validation && msg.fields.routingKey === "out.app." + message.data.validation.token + ".token.refresh") {
                    if (message.data && !message.data.errors) {
                        channel.bindQueue(q.queue, exchange, "out.app." + message.data.refreshTokens.accessToken + ".#");
                        io.socket.emit("app:token:refresh", message);
                    }
                }
            }, {
                noAck: false
            });
        });
    });
});

