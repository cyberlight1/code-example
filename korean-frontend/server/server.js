const express = require("express");
const path = require('path');
const fs = require('fs');

const cors = require("cors");

const app = express();

const rabbitMQ = require('./rabbitMQ');

const bodyParser = require('body-parser');

const DIST_DIR = path.join(__dirname, '../dist');
const HTML_FILE = path.join(DIST_DIR, './index.html');

app.use(cors({
    exposedHeaders: ['Content-Length', 'Content-Type'],
}));

app.use(express.static(DIST_DIR));

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

// API

app.post('/api/register', async (req, res) => {
    try {
        const data = req.body;

        let query = "mutation { registerUser(login: \"" + data.login + "\", email: \"" + data.email + "\", password: \"" + data.password + "\") { login } }"
        let headers = { headers: {} }

        rabbitMQ("in.register", query, headers);

        return res.status(200).json({ status: "succesfully register" });
    } catch (error) {
        res.status(500).send(error);
    }
})

app.post('/api/login', async (req, res) => {
    try {
        const data = req.body;

        let query = "query { userLogin(login: \"" + data.login + "\", password: \"" + data.password + "\") { id, login } }"

        let headers = { headers: {} }

        rabbitMQ("in.login", query, headers);

        return res.status(200).json({ status: "succesfully login" });
    } catch (error) {
        res.status(500).send(error);
    }
})

app.post('/api/user', async (req, res) => {
    try {
        const data = req.body;

        let query = "query { user(login: \"" + data.login + "\") { id, login, password } }"
        let headers = {
            headers: {
                access_token: data.access_token
            }
        }

        rabbitMQ("in.app." + data.access_token + ".user", query, headers);

        return res.status(200).json({ status: "succesfully get user" });
    } catch (error) {
        res.status(500).send(error);
    }
})

app.post('/api/token_verify', async (req, res) => {
    try {
        const data = req.body;

        let query = "query { verifyTokens(accessToken: \"" + data.access_token + "\") { accessToken } }"
        let headers = {
            headers: {
                access_token: data.access_token
            }
        }

        rabbitMQ("in.app." + data.access_token + ".token.verify", query, headers);

        return res.status(200).json({ status: "succesfully login" });
    } catch (error) {
        res.status(500).send(error);
    }
})

app.post('/api/token_refresh', async (req, res) => {
    try {
        const data = req.body;

        let query = "mutation { refreshTokens(userId: " + data.user_id + ", accessToken: \"" + data.access_token + "\", refreshToken: \"" + data.refresh_token + "\") { accessToken, refreshToken } }"
        let headers = {
            headers: {
                access_token: data.access_token
            }
        }

        rabbitMQ("in.app." + data.access_token + ".token.refresh", query, headers);

        return res.status(200).json({ status: "succesfully login" });
    } catch (error) {
        res.status(500).send(error);
    }
})

app.post('/api/users', async (req, res) => {
    try {
        const data = req.body;

        let query = "query { users { id, login, email } }"
        let headers = {
            headers: {
                access_token: data.access_token
            }
        }
        
        rabbitMQ("in.app." + data.access_token + ".user.all", query, headers);

        return res.status(200).json({ status: "succesfully get all users" });
    } catch (error) {
        res.status(500).send(error);
    }
})

// FRONTEND

app.get('*', (req, res) => {
    res.sendFile(HTML_FILE);
});

const server = require('http').Server(app);

const io = require('socket.io')(server);

module.exports = {
    socket: any = io.on('connection', (_) => {}),
};


server.listen(8080);
