import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';

export const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#b8d0ef',
            main: '#a7c5eb',
            dark: '#949cdf',
            contrastText: '#1b1b1b',
        }
    },
});