import React, { Component } from 'react';
import './App.css';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Home from "./components/Home";
import Login from "./components/Account/Login";
import Register from "./components/Account/Register";

localStorage.setItem('main_url', "http://localhost:8080");


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/register" component={Register} />
                    <Route path="/user" component={Login} />
                    <Route exact path="/" component={Home} />
                </Switch>
            </Router>
        )
    }
}

export default App;
