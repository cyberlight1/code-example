import React, { Component } from 'react';

import { GoogleReCaptchaProvider, GoogleReCaptcha } from 'react-google-recaptcha-v3';

import { withStyles, ThemeProvider } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';

import Alert from '@material-ui/lab/Alert';

import { theme } from '../../Theme';
import '../../App.css';

import LoginLogo from "../../../assets/logo.webp";

import socketIOClient from "socket.io-client";
const ENDPOINT = localStorage.getItem('main_url');


const styles = {
    box: {
        backgroundColor: "#cd5d7d",
        marginTop: "100px",
        minHeight: "600px",
        borderRadius: "0 20px 20px 20px",
        boxShadow: "0px 0px 10px -5px #000000"
    },
    logo: {
        minHeight: "128px",
        minWidth: "128px",
        backgroundColor: "#ffffff",
        transform: "translate(-24px, -64px)",
        boxShadow: "0px 0px 10px -5px #000000",
    },
    logoImg: {
        maxWidth: "100px",
        backgroundColor: "#ffffff"
    },
    loginForm: {
        marginTop: "-40px"
    },
    linksContainer: {
        marginTop: "10px",
    },
    links: {
        color: "#1b1b1b",
    },
    field: {
        backgroundColor: "#f6ecf0",
        borderRadius: "4px",
    },
    button: {
        marginTop: "30px"
    },
    errorsContainer: {
        marginTop: "10px",
    },
};

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: "",
            email: "",
            password: "",
            confirm_password: "",
            password_not_match: false,
            token: "",
            status: undefined
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVerify = this.handleVerify.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(this.state.login !== nextState.login) {
            return true;
        }
        if(this.state.email !== nextState.email) {
            return true;
        }
        if(this.state.password !== nextState.password) {
            return true;
        }
        if(this.state.confirm_password !== nextState.confirm_password) {
            return true;
        }
        if(this.state.password_not_match !== nextState.password_not_match) {
            return true;
        }
        if(this.state.token !== nextState.token ) {
            return true;
        }

        return false;
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({
            ...this.state,
            [event.target.name]: value
        });
    }

    handleVerify(token) {
        this.setState({
            ...this.state,
            token: token
        })
    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.state.password == this.state.confirm_password && this.state.token.length > 0) {
            fetch('/api/register', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ login: this.state.login, email: this.state.email, password: this.state.password })
            }).then(res =>{
                if (res.status === 200) {
                    this.setState({
                        ...this.state,
                        status: true
                    })
                } else {
                    this.setState({
                        ...this.state,
                        status: false
                    })
                }
            });
        } else {
            this.setState({
                ...this.state,
                password_not_match: !this.state.password_not_match
            });
        }
    }

    render() {
        return (
            <GoogleReCaptchaProvider reCaptchaKey="6LeT9S0aAAAAAOO0IwTcUSt5VthRwu2RzdfdkwPJ">
                <ThemeProvider theme={theme}>
                    <Container component="main" maxWidth="xs" className={this.props.classes.box}>
                    <Link href="/user">
                        <Avatar alt="Logo" className={this.props.classes.logo}>
                            <img src={LoginLogo} className={this.props.classes.logoImg}></img>
                        </Avatar>
                    </Link>

                        <Grid
                            container
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                            className={this.props.classes.loginForm}
                        >

                            <Typography component="h1" variant="h5">
                                Zarejestruj się
                        </Typography>

                            <form className="" noValidate method="POST" onSubmit={this.handleSubmit}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="login"
                                    label="Login"
                                    name="login"
                                    autoComplete="current-login"
                                    autoFocus
                                    className={this.props.classes.field}
                                    value={this.state.login}
                                    onChange={this.handleChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    name="email"
                                    autoComplete="current-email"
                                    autoFocus
                                    className={this.props.classes.field}
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Hasło"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    className={this.props.classes.field}
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="confirm_password"
                                    label="Powtórz hasło"
                                    type="password"
                                    id="confirm-password"
                                    autoComplete="current-confirm-password"
                                    className={this.props.classes.field}
                                    value={this.state.confirm_password}
                                    onChange={this.handleChange}
                                />
                                {
                                    this.state.password_not_match ?
                                        <Alert severity="error" className={this.props.classes.errorsContainer}>Hasła nie są takie same</Alert> : 
                                        <></>
                                }
                                <GoogleReCaptcha onVerify={this.handleVerify} />
                                {
                                    this.state.status !== undefined ?
                                        this.state.status ?
                                            <Alert severity="success">Użytkownik zarejestrowany</Alert> :
                                            <Alert severity="error">Błąd rejestracji, spróbuj ponownie</Alert> :
                                        undefined
                                }
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={this.props.classes.button}
                                >
                                    Zarejestruj się
                            </Button>
                            </form>
                        </Grid>
                    </Container>
                </ThemeProvider>
            </GoogleReCaptchaProvider>
        );
    }
}

export default withStyles(styles)(Register);
