import React, { Component } from 'react';

import { withStyles, ThemeProvider } from '@material-ui/core/styles';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Alert from '@material-ui/lab/Alert';

import { theme } from '../../Theme';
import '../../App.css';

import LoginLogo from "../../../assets/logo.webp";

import Admin from "../Admin/Admin";

import socketIOClient from "socket.io-client";
const ENDPOINT = localStorage.getItem('main_url');
const socket = socketIOClient(ENDPOINT)

const styles = {
    box: {
        backgroundColor: "#cd5d7d",
        marginTop: "100px",
        minHeight: "500px",
        borderRadius: "0 20px 20px 20px",
        boxShadow: "0px 0px 10px -5px #000000"
    },
    logo: {
        minHeight: "128px",
        minWidth: "128px",
        backgroundColor: "#ffffff",
        transform: "translate(-24px, -64px)",
        boxShadow: "0px 0px 10px -5px #000000",
    },
    logoImg: {
        maxWidth: "100px",
        backgroundColor: "#ffffff"
    },
    loginForm: {
        marginTop: "-40px"
    },
    linksContainer: {
        marginTop: "10px",
    },
    links: {
        color: "#1b1b1b",
    },
    field: {
        backgroundColor: "#f6ecf0",
        borderRadius: "4px",
    },
    button: {
        marginTop: "30px"
    },
    errorsContainer: {
        marginTop: "10px",
    },
};

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: "",
            password: "",
            remember: false,
            access_token: "",
            refresh_token: "",
            user: {},
            errors: {},
            update_user: false,
            loggedin: false,
            loggedout: false,
            submit: false,
        };

        this.logout = this.logout.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    logout() {
        this.setState({
            ...this.state,
            loggedout: true,
            loggedin: false,
        })
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({
            ...this.state,
            [event.target.name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({
            ...this.state,
            errors: {},
            loggedout: false,
            update_user: !this.state.update_user,
            submit: true,
        })

        socket.on("app:errors", msg => {
            if (msg.data && msg.data.errors && msg.data.errors.errorMessage && msg.data.errors.errorMessage.length > 0) {
                this.setState({
                    ...this.state,
                    errors: msg.data.errors,
                })
            }
        });

        fetch('/api/login', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({ login: this.state.login, password: this.state.password })
        });

        socket.on("app:tokens", msg => {
            if (msg.data && msg.data.token) {
                this.setState({
                    ...this.state,
                    access_token: msg.data.token.accessToken,
                    refresh_token: msg.data.token.refreshToken,
                    loggedin: !this.state.loggedin,
                })
                if (this.state.remember) {
                    localStorage.setItem('access_token', msg.data.token.accessToken);
                    localStorage.setItem('refresh_token', msg.data.token.refreshToken);
                }
            }
        });

        this.renderUser();
    }

    renderUser() {
        if (Object.keys(this.state.user).length === 0) {
            socket.on("errors", msg => {
                if (msg.data && msg.data.errors) {
                    this.setState({
                        ...this.state,
                        errors: msg.data.errors,
                    })
                }
            });

            if (!this.state.update_user) {
                fetch('/api/user', {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({ login: this.state.login, access_token: this.state.access_token })
                }).then(_ => {
                    this.setState({
                        ...this.state,
                        update_user: !this.state.update_user,
                    })
                });
            }

            if (this.state.update_user) {
                socket.on("app:users", msg => {
                    this.setState({
                        ...this.state,
                        user: msg.data.user,
                    })
                    if (this.state.remember) {
                        sessionStorage.removeItem('user')
                        localStorage.setItem('user', JSON.stringify(msg.data.user));
                    } else {
                        localStorage.removeItem('user')
                        sessionStorage.setItem('user', JSON.stringify(msg.data.user));
                    }
                });
            }
        }
    }

    reloadTokens() {
        if (!this.state.loggedin && localStorage.getItem('access_token') !== null && localStorage.getItem('refresh_token') !== null) {
            this.setState({
                ...this.state,
                access_token: localStorage.getItem('access_token'),
                refresh_token: localStorage.getItem('refresh_token'),
                loggedin: true,
            })
        }
    }

    render() {
        this.reloadTokens();

        if (this.state.loggedin) {
            return <Admin
                logout={this.logout}
                user={this.state.user}
                access_token={this.state.access_token}
                refresh_token={this.state.refresh_token}
            />
        }

        return (
            <ThemeProvider theme={theme}>
                <Container component="main" maxWidth="xs" className={this.props.classes.box}>
                    <Link href="/">
                        <Avatar alt="Logo" className={this.props.classes.logo}>
                            <img src={LoginLogo} className={this.props.classes.logoImg}></img>
                        </Avatar>
                    </Link>

                    <Grid
                        container
                        direction="row"
                        justify="flex-start"
                        alignItems="center"
                        className={this.props.classes.loginForm}
                    >

                        <Typography component="h1" variant="h5">
                            Zaloguj się
                        </Typography>


                        <form className="" noValidate method="POST" onSubmit={this.handleSubmit}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="login"
                                label="Login"
                                name="login"
                                autoComplete="current-login"
                                autoFocus
                                className={this.props.classes.field}
                                value={this.state.login}
                                onChange={this.handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Hasło"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                className={this.props.classes.field}
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                            {
                                this.state.errors && this.state.errors.errorMessage && this.state.errors.errorMessage.length > 0 ?
                                    <Alert severity="error" className={this.props.classes.errorsContainer}>{this.state.errors.errorMessage}</Alert> :
                                    <></>
                            }
                            <FormControl>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            id="remember"
                                            name="remember"
                                            onChange={e => {
                                                this.setState({ remember: e.target.checked });
                                            }}
                                        />
                                    }
                                    label="Zapamiętaj logowanie"
                                />
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={this.props.classes.button}
                            >
                                Zaloguj
                            </Button>
                            <Grid container className={this.props.classes.linksContainer}>
                                <Grid item xs>
                                    <Link href="#" className={this.props.classes.links}>
                                        Nie pamiętasz hasła?
                                </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="/register" className={this.props.classes.links}>
                                        Zarejestruj się
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </Grid>
                </Container>
            </ThemeProvider>
        );
    }
}

export default withStyles(styles)(Login);
