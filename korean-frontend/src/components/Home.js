import React, { Component } from 'react';

import { ThemeProvider } from '@material-ui/core/styles';

import { theme } from '../Theme';
import HomeHeader from "./HomeHeader";

const styles = {
};

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
                <HomeHeader />
            </ThemeProvider>
        );
    }
}

export default Home;
