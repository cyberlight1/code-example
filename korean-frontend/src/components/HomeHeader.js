import React, { Component } from 'react';

import { withRouter } from 'react-router';
import { withStyles, ThemeProvider } from '@material-ui/core/styles';

import Link from '@material-ui/core/Link';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';

import { theme } from '../Theme';
import LoginLogo from "../../assets/logo.webp";


const styles = {
    menu: {
        backgroundColor: "#cd5d7d",
    },
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        paddingLeft: "20px",
    },
    logo: {
        minHeight: "64px",
        minWidth: "64px",
        backgroundColor: "#ffffff",
        boxShadow: "0px 0px 10px -5px #000000",
    },
    logoImg: {
        maxWidth: "50px",
        backgroundColor: "#ffffff"
    },
}

class HomeHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_menu_show: false,
            anchorel: undefined,
        };

        this.showMenu = this.showMenu.bind(this);
        this.loginMe = this.loginMe.bind(this);
    }

    showMenu(event) {
        this.setState({
            ...this.state,
            is_menu_show: !this.state.is_menu_show,
            anchorel: event.currentTarget,
        })
    }

    loginMe() {
        this.props.history.push("/user")
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
                <AppBar position="sticky">
                    <Toolbar className={this.props.classes.menu}>
                        <Link href="/">
                            <Avatar alt="Logo" className={this.props.classes.logo}>
                                <img src={LoginLogo} className={this.props.classes.logoImg}></img>
                            </Avatar>
                        </Link>
                        <Typography variant="h6" className={this.props.classes.title}>Korean Learning Platform</Typography>
                        <Menu id="simple-menu" keepMounted open={this.state.is_menu_show} anchorEl={this.state.anchorel}>
                            <MenuItem onClick={this.loginMe}>Zaloguj</MenuItem>
                        </Menu>

                        <IconButton edge="end" className={this.props.classes.menuButton} color="inherit" onClick={this.showMenu}>
                            <MenuIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </ThemeProvider>
        );
    }
}

export default withRouter(withStyles(styles)(HomeHeader));
