import React, { Component } from 'react';

const yaml = require('js-yaml');

import { withStyles, ThemeProvider } from '@material-ui/core/styles';

import { theme } from '../../Theme';

import Header from "./Header";
import Sidebar from "./Sidebar";

import Users from './Users/Users';
import Teams from './Teams/Teams';

import socketIOClient from "socket.io-client";
const ENDPOINT = localStorage.getItem('main_url');
const socket = socketIOClient(ENDPOINT)


const styles = {

}

class Admin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            access_token: "",
            refresh_token: "",
            user: {},
            errors: {},
            update_access_token: false,
            update_refresh_token: false,
            update_user_rows: false,
            access_token_valid: {},
            showUsers: false,
            showTeams: false,
            users_data: {},
            token_rendered: false,
        };

        this.renderPartial = this.renderPartial.bind(this)
    }

    componentDidMount() {
        this.setState({
            ...this.state,
            user: this.props.user,
            access_token: this.props.access_token,
            refresh_token: this.props.refresh_token,
        })
    }

    renderPartial(element) {
        if (element === "users") {
            this.setState({
                ...this.state,
                showUsers: !this.state.showUsers,
            })
        } else if (element === "teams") {
            this.setState({
                ...this.state,
                showTeams: !this.state.showTeams,
            })
        }
    }

    renderToken() {
        if (!this.state.token_rendered) {
            this.setState({
                ...this.state,
                token_rendered: true,
            })
            
            socket.on("errors", msg => {
                if (msg.data && msg.data.errors) {
                    this.setState({
                        ...this.state,
                        errors: msg.data.errors,
                    })
                }
            });

            if (!this.state.update_access_token) {
                fetch('/api/token_verify', {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({ user_id: this.state.user.id, access_token: this.state.access_token })
                }).then(_ => {
                    this.setState({
                        ...this.state,
                        update_access_token: !this.state.update_access_token,
                    })
                });
            }

            if (this.state.update_access_token) {
                socket.on("app:token:verify", msg => {
                    this.setState({
                        ...this.state,
                        access_token_valid: msg.data.accessToken,
                    })
                });

            }

            if (Object.keys(this.state.errors).length === 0 && !this.state.update_refresh_token) {
                fetch('/api/token_refresh', {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({ user_id: this.state.user.id, access_token: this.state.access_token, refresh_token: this.state.refresh_token })
                }).then(_ => {
                    this.setState({
                        ...this.state,
                        update_refresh_token: !this.state.update_refresh_token,
                    })
                });
            }

            if (Object.keys(this.state.errors).length === 0 && this.state.update_refresh_token) {
                socket.on("app:token:refresh", msg => {
                    if (this.state.access_token !== msg.data.refreshTokens.accessToken) {
                        localStorage.setItem('access_token', msg.data.refreshTokens.accessToken);
                        localStorage.setItem('refresh_token', msg.data.refreshTokens.refreshToken);
                    }
                    this.setState({
                        ...this.state,
                        access_token: msg.data.refreshTokens.accessToken,
                        refresh_token: msg.data.refreshTokens.refreshToken,
                    })
                });

            }
        }
    }

    setUserTableRows() {
        socket.on("errors", msg => {
            if (msg.data && msg.data.errors) {
                this.setState({
                    ...this.state,
                    errors: msg.data.errors,
                })
            }
        });

        if (!this.state.update_user_rows) {
            fetch('/api/users', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({ access_token: this.state.access_token })
            }).then(_ => {
                this.setState({
                    ...this.state,
                    update_user_rows: !this.state.update_user_rows,
                })
            });
        }

        if (this.state.update_user_rows) {
            socket.on("app:users:all", msg => {
                this.setState({
                    ...this.state,
                    users_data: msg,
                })
            });
        }
    }

    render() {
        if (this.state.access_token.length > 0) {
            this.renderToken();
        }

        if (Object.keys(this.state.users_data).length === 0) {
            this.setUserTableRows();
        }

        if (this.state.access_token.length === 0) {
            return <>
                Błąd HTTP 401.1 — Dostęp nieautoryzowany
            </>
        }

        return (
            <ThemeProvider theme={theme}>
                <Header user={this.state.user} logout={this.props.logout} />
                <Sidebar action={this.renderPartial} />
                <Users show={this.state.showUsers} users={this.state.users_data} />
                <Teams show={this.state.showTeams} />
            </ThemeProvider>
        );
    }
}

export default withStyles(styles)(Admin);
