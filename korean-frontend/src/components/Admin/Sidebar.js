import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { withStyles, ThemeProvider } from '@material-ui/core/styles';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/List';
import ListItemText from '@material-ui/core/List';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

import MenuBookIcon from '@material-ui/icons/MenuBook';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PersonIcon from '@material-ui/icons/Person';
import GroupIcon from '@material-ui/icons/Group';
import AssistantIcon from '@material-ui/icons/Assistant';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import ChatIcon from '@material-ui/icons/Chat';

import { theme } from '../../Theme';

import { Typography } from '@material-ui/core';


const styles = {
    panel: {
        width: 250,
    },
    list: {
        width: 250,
        height: "100vh",
        backgroundColor: "#cd5d7d"
    },
    listItemButton: {
        width: 250,
        justifyContent: "flex-start",
        alignItems: "normal",
        textTransform: "capitalize"
    },
    listItem: {
        display: "inline-flex",
        marginLeft: "10px",
        width: "95%",
    },
    arrowIcon: {
        backgroundColor: "#cd5d7d",
    },
    listItemText: {
        marginLeft: "10px",
    }
}

class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_sidebar_show: false,
            anchorel: undefined,
        };

        this.showSidebar = this.showSidebar.bind(this);
    }

    showSidebar(event) {
        this.setState({
            ...this.state,
            is_sidebar_show: !this.state.is_sidebar_show,
            anchorel: event.currentTarget,
        })
    }

    render() {
        return ( 
            <ThemeProvider theme={theme}>
                <Tooltip title="Panel">
                    <IconButton onClick={this.showSidebar}>
                        <Typography>Rozwiń panel</Typography>
                        <ArrowForwardIcon className={this.props.classes.arrowIconText} />
                    </IconButton>
                </Tooltip>
                <Drawer open={this.state.is_sidebar_show} anchorEl={this.state.anchorel} className={this.props.classes.panel}>
                    <Tooltip title="Panel">
                        <IconButton onClick={this.showSidebar}>
                            <ArrowBackIcon className={this.props.classes.arrowIconText} />
                            <Typography>Zwiń panel</Typography>
                        </IconButton>
                    </Tooltip>
                    <List className={this.props.classes.list}>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('users')} >
                                <ListItemIcon><PersonIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Użytkownicy</ListItemText>
                            </Button>
                        </ListItem>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('teams')} >
                                <ListItemIcon><GroupIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Drużyny</ListItemText>
                            </Button>
                        </ListItem>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('lessons')}>
                                <ListItemIcon><MenuBookIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Lekcje</ListItemText>
                            </Button>
                        </ListItem>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('badges')}>
                                <ListItemIcon><AssistantIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Odznaki</ListItemText>
                            </Button>
                        </ListItem>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('certs')}>
                                <ListItemIcon><VerifiedUserIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Certyfikaty</ListItemText>
                            </Button>
                        </ListItem>
                        <ListItem className={this.props.classes.listItem}>
                            <Button className={this.props.classes.listItemButton} onClick={() => this.props.action('chats')}>
                                <ListItemIcon><ChatIcon /></ListItemIcon>
                                <ListItemText className={this.props.classes.listItemText}>Czat</ListItemText>
                            </Button>
                        </ListItem>
                    </List>
                </Drawer>
            </ThemeProvider >
        );
    }
}

export default withStyles(styles)(Sidebar);
