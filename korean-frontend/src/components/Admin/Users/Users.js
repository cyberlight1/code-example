import React, { Component } from 'react';

const yaml = require('js-yaml');

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { withStyles } from '@material-ui/core/styles';

import socketIOClient from "socket.io-client";
const ENDPOINT = localStorage.getItem('main_url');


const styles = {

}

class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        if (this.props.show) {
            return (
                <TableContainer>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Login</TableCell>
                                <TableCell>E-mail</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow key="0">
                                <TableCell component="th" scope="row">
                                </TableCell>
                                <TableCell></TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                            {
                                this.props.users.length > 0 ?
                                    this.props.users.map((row) => (
                                        <TableRow key={row.id}>
                                            <TableCell component="th" scope="row">
                                                {row.id}
                                            </TableCell>
                                            <TableCell>{row.login}</TableCell>
                                            <TableCell>{row.email}</TableCell>
                                        </TableRow>)) :
                                    <></>
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        }

        return <></>
    }
}

export default withStyles(styles)(Users);
